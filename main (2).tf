// Configure the Google Cloud provider
provider "google" {
 credentials = "${file("My First Project-a04da22364ac.json")}"
 project     = "zeta-dock-216820"
 region      = "europe-west1-b"
}