// Terraform plugin for creating random ids
resource "random_id" "instance_id" {
 byte_length = 8
}

// A single Google Cloud Engine instance
resource "google_compute_instance" "26Academy" {
 name         = "flask-vm-${random_id.instance_id.hex}"
 machine_type = "n1-standard-2"
 zone         = "europe-west1-b"

 boot_disk {
   initialize_params {
     image = "ubuntu-1604-xenial-v20190306" 
   }
 }
 
// Make sure flask is installed on all new instances for later steps
 metadata_startup_script = "sudo apt update; sudo apt install -y docker.io; sudo usermod -aG docker $USER;sudo docker run --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.11 http://10.132.0.7/v1/scripts/7E2740D21BF21FF2AD8F:1546214400000:Auep46zRCMsnfh3pXtQgviQDsw"

 network_interface {
   network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
}
// A single Google Cloud Engine instance
resource "google_compute_instance" "default" {
 name         = "flask-vm-lesulis"
 machine_type = "n1-standard-2"
 zone         = "europe-west1-b"

 boot_disk {
   initialize_params {
     image = "ubuntu-1604-xenial-v20190306"
   }
 }

// Make sure flask is installed on all new instances for later steps
 metadata_startup_script = "sudo apt update; sudo apt install -y docker.io; sudo usermod -aG docker $USER;sudo docker run --rm --privileged -v /var/run/docker.sock:/var/run/docker.sock -v /var/lib/rancher:/var/lib/rancher rancher/agent:v1.2.11 http://10.132.0.7/v1/scripts/7E2740D21BF21FF2AD8F:1546214400000:Auep46zRCMsnfh3pXtQgviQDsw"

 network_interface {
   network = "default"

   access_config {
     // Include this section to give the VM an external ip address
   }
 }
}